# Ansible common role
This role aims to install and set up basics on hosts.

## Dependencies
This role depends on the following roles:
* https://github.com/jpiron/ssmtp-role.git for the SSMTP configuration.
* https://github.com/willshersystems/ansible-sshd.git for SSH configuration.
* https://github.com/Oefenweb/ansible-locales.git for locales configuration.

See meta/main.yml for more information.

## Role variables (see defaults/main.yml for default values)
### General configuration
* `common_locale`: Locale to be configured
* `common_locales_present`: Locale to be installed
* `common_locales_default`: Default values for individual LC_* values. See: https://github.com/Oefenweb/ansible-locales.
* `common_swappiness`: Swappiness to be configured.
* `common_pkg_state`: Specifies whether to only intall the packages or to intall and update them.
* `common_pkg_list_deb`: List of packages to be installed.
* `common_extra_packages`: List of extra packages to be installed. Give the ability to define packages to be installed as a per host basis.
* `common_pkg_purge`: Whether to delete configuration file on package removal or not.
* `common_pkg_list_deb_remove`: List of packages to be removed.
* `common_motd`: Toggle motd deployment.
* `common_timezone`: Timezone to be configured.

### Network configuration
* `common_hostname`: The hostname of the server(s) to be configured. Default to {{ ansible_hostname }}. This should be overriden at inventory level.
* `common_domainame`: The dnsdomain name of the server(s) to be configured. This should be overriden at inventory level.
* `common_etc_hosts_configuration`: Whether or not to configure /etc/hosts.
* `common_etc_hosts_extra_entries`: Extra entries fo rthe /etc/hosts file. Default localhost entries are handled directly within the template.
* `common_network_interfaces`: A list of network interfaces to be configured. Each network interface:
  * must define:
    * name. eth0, eth1, ...
    * type. inet, inet6, ... .Default to `inet`.
    * configuration_kind. static, dhcp, loopback, ... . Default to `static`.
    * address (when configuration_kind is not dhcp).
    * netmask (when configuration_kind is not dhcp).
  * can define:
    * network. IP address of the network.
    * broadcast. IP address of the broadcast.
    * gateway. IP address of the gateway.
    * metric. The routing metric for the gateway.
    * dns_nameservers. A **list** of nameservers to be configured.
    * dns_search. A **list** for host-name lookup.
    * extras. A **list** of strings that will be append untouched to the interface configuration (e.g: `"up ip route add 10.0.0.0/8 via 10.0.0.1 dev eth0"`)

### SSH configuration
* `common_ssh_configuration`: Whether or not to configure SSH.

See https://github.com/willshersystems/ansible-sshd for configuration items.

However, with the [ssh-audit](https://github.com/arthepsy/ssh-audit) recommendations along with the [securiser-secure-shell-ssh](https://www.guillaume-leduc.fr/securiser-secure-shell-ssh.html) ones, a secure SSH configuration could be:
```yaml
sshd_KexAlgorithms: curve25519-sha256@libssh.org
sshd_HostKey:
  - /etc/ssh/ssh_host_rsa_key
  - /etc/ssh/ssh_host_ed25519_key
sshd_PasswordAuthentication: no
sshd_ChallengeResponseAuthentication: no
sshd_PubkeyAuthentication: yes
sshd_AllowGroups: ssh-users  # Make sure to add your users to this group !
sshd_Ciphers: chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
sshd_MACs: hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com
```

### SSMTP configuration
* `common_ssmtp_configuration`: Whether or not to configure SSMTP.
* `common_ssmtp_postmaster`: Recipient for all emails sent to user with UID under 1000.
* `common_ssmtp_mailhub_host`: The SMTP host.
* `common_ssmtp_mailhub_port`: The SMTP port.
* `common_ssmtp_auth_user`: The username to authenticate on the SMTP.
* `common_ssmtp_auth_password`: The password to authenticate on the SMTP.
* `common_ssmtp_use_tls`: Whether to use TLS or not. Use "YES" with the quotes to activate it.
* `common_ssmtp_starttls`: Whether to use STARTTL or not. Use "YES" with the quotes to activate it.
* `common_ssmtp_rewrite_domain`: See SSMTP documentation.
* `common_ssmtp_from_line_override`: See SSMTP documentation.
* `common_ssmtp_hostname`: See SSMTP documentation.
* `common_ssmtp_authorized_users`: See SSMTP documentation.

### Authentication configuration
* `common_users`: A list of users to create. Each user :
  * must define:
    * name
  * can define:
    * password (mandatory if state is present)
    * group. The user primary group.
    * groups. A list of its secondary groups.
    * append. Whether to append groups or not. Default to `common_users_defaults.append`.
    * createhome. Default to `common_users_defaults.createhome`.
    * home. Default to `undefined`.
    * shell. Default to `common_users_defaults.shell`.
    * system. Whether or not to create a system user/ Default to `common_users_defaults.system`.
    * update_password. Policy to define whether to always update password or not. Default to `common_users_defaults.update_password`.
    * state. `present` or `absent`. Default to `common_users_defaults.state`.
    * ssh_unauthorized_keys. A list of ssh public key to remove from authorized ones. Default to `common_users_defaults.ssh_unauthorized_keys`. Special `ALL` value will remove all keys. Unauthorized keys are removed before authorized ones are added.
    * ssh_authorized_keys. A list of ssh public key to authorized login for. Default to `common_users_defaults.ssh_authorized_keys`.
* `common_users_defaults`: Default values for users to create.
* `common_groups`: A list of groups to create. Each group :
  * must define:
    * name
  * can define:
    * gid. The group gid.
    * state. Default to `common_groups_defaults.state`.
    * system. Whether the group is a system group or not. Default to `common_groups_defaults.system`.
* `common_groups_defaults`: Default values for groups to create.

### Mounts/devices configuration
* `common_mounts`: A list of mounts to manage. Each mount :
  * must define:
    * mountpoint. The directory where to perform the mount.
    * device. The device to be mounted. Prefer UUID notation.
    * mount_fstype. The filesystem type (e.g ext4, nfs, ...)
  * can define:
    * mount_opts. Default to `common_mounts_defaults.mount_opts`.
    * mount_dump. Default to `common_mounts_defaults.mount_dump`.
    * mount_passno. Default to `common_mounts_defaults.mount_passno`.
    * mount_state. Default to `common_mounts_defaults.mount_state`.
* `common_mounts_defaults`: Default values for mounts to manage.
