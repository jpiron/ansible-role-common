# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
## Removed
- ntp configuration

## [3.3.3] - 2020-05-10
## Changed
- Update ansible-locales role.

## [3.3.1] - 2019-11-05
## Fixed
- Ansible 2.9 compatibility.

## [3.3.0] - 2019-11-04
## Changed
- Now under WTFPL license.
- Update some dependencies.

## [3.1.0] - 2018-06-18
## Changed
- Minor dependencies upgrades.

## [3.0.0] - 2018-02-20
## Changed
- Improve user's default value definition.
- Improve SSH authorized keys removal management.

## [2.5.2] - 2017-10-13
## Changed
- Minor dependencies upgrades.

## [2.5.1] - 2017-09-14
## Changed
- Minor dependencies upgrades.

## [2.5.0] - 2017-09-06
## Changed
- Enable user's defined authorized ssh keys exclusivity configuration.

## [2.4.0] - 2017-08-10
### Changed
- Fix dhcp network interfaces configuration.

### Removed
- Remove default values for network interfaces adresses and netmasks.

## [2.3.0] - 2017-08-10
### Added
- Add network interface metric option management

### Removed
- Remove no longer used handlers.

## [2.2.1] - 2017-08-09
### Changed
- Fix timezone incomplete and sometimes bugged management.

## [2.2.0] - 2017-06-14
### Added
- Add common_ssmtp_postmaster configuration item.
- Add locales management.

### Changed
- Use the Ansible sysctl module to set the swappiness.
- Upgrade sshd role from 0.4.7 to 0.5.
- Switch back to legit ssmtp-role role.

### Removed
- Remove the umask configuration item for users.

## [2.1.1] - 2017-04-05
### Changed
- Ansible When statements.

### Removed
- Remove become statements from tasks.

## [2.1.0] - 2016-12-27
### Changed
- Upgrade sshd role from 0.4.6 to 0.4.7.

### Removed
- Remove sudoers configuration.

## [2.0.1] - 2016-12-21
### Added
- Add a SSH configuration switch item.

## [2.0.0] - 2016-12-18
### Added
- Add a umask configuration item for users.
- Add a system configuration item for groups.

### Changed
- Replace Postfix by SSMTP.
- Replace in-role SSH configuration with an external role.

## [1.1.0] - 2016-12-11
### Added
- Handle Postix configuration.

### Changed
- Use an existing Ansible role to configure NTP.
- Ansible minimum required version is now 2.0.
- Better variables split in the README.
